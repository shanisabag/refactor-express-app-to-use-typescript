import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import usersRoute from "./routes/Users/users.js";

const { PORT, HOST = 'localhost' } = process.env;

const app = express();

app.use(morgan('dev'));

// parse application/json
app.use(express.json());

app.use("/users", usersRoute);

app.get("/", (req, res) => {
    res.status(200).json("home page");
});

app.listen(Number(PORT), HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});