import express from "express";
import log from "@ajar/marker";

const router = express.Router();

const usersMiddleware = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    log.yellow("middleware - users");
    next();
}

router.use(usersMiddleware);

router.get('/', (req, res) => {
    res.status(200).json("all users");
});

router.get('/:userId', (req, res) => {
    res.status(200).json(`userId = ${req.params.userId}`);
});

export default router;


